//
//  Constants.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatClientSyncController
import Foundation

enum Constants {
    static let clientConfig = ClientConfig(scheme: .http,
                                           host: "localhost",
                                           port: 8080)
}
