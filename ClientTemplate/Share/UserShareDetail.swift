//
//  UserShareDetail.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 30/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatCore

struct UserShareDetail: View {
    @UserDefaultOptionalWrapper(.username)
    private var usernameStorage: String?
    let username: String

    @State
    var selectedRole: Role

    var roleUpdated: ((String, Role) -> Void)? = nil

    func setRole(to role: Role) {
        selectedRole = role
        roleUpdated?(username, role)
    }

    func roleDescription(for role: Role) -> String {
        if role == .owner {
            return "Owner"
        } else {
            return "Member"
        }
    }

    var body: some View {

        HStack {
            Text(username)
            Spacer()
            if username == usernameStorage {
                Text(roleDescription(for: selectedRole) + " (you)")
                    .foregroundColor(.secondary)
            } else if roleUpdated != nil {
                Picker(selection: .init(get: { self.selectedRole }, set: self.setRole), label: Text("Role")) {
                    Text(roleDescription(for: .readAndWrite))
                        .tag(Role.readAndWrite)
                    Text(roleDescription(for: .owner))
                        .tag(Role.owner)
                }.pickerStyle(SegmentedPickerStyle()).frame(width: 160)
            } else {
                Text(roleDescription(for: selectedRole))
                    .foregroundColor(.secondary)
            }
        }
    }
}

struct UserShareDetail_Previews: PreviewProvider {
    static var previews: some View {
        UserShareDetail(username: "Mark", selectedRole: .owner) { _, _ in

        }
    }
}
