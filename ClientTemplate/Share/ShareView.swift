//
//  ShareView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 30/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper
import MeerkatClientSyncController

struct ShareView: View {
    @UserDefaultOptionalWrapper(.username)
    private var user: String?

    @ObservedObject
    var group: ObservableSyncObject<SyncGroup>

    @ObservedObject
    private var client = MeerkatClientAPI(clientConfig: Constants.clientConfig)

    @ObservedObject
    private var authStatus = SyncAuthStatus.shared

    @State
    private var username = ""

    private let users: [Tmp] = []

    @State
    private var task: Cancellable? = nil

    @State
    private var alertType: AlertType?

    private let isOwner = true

    var body: some View {
        NavigationView {
            VStack {
                if isOwner {
                    VStack {
                        HStack {
                            Text("Add a user:")
                            Spacer()
                        }
                        TextField("Username", text: $username)
                            .modifier(FormRowModifier())
                        Button(action: add) {
                            Text("Add")
                        }.modifier(FormButtonModifier(color: .green))
                    }.padding()
                }
                HStack {
                    Text("Users:")
                    Spacer()
                }.padding(.leading)
                    .padding(.bottom, -12)
                LoadingView(isShowing: $client.isLoading, cancel: cancelTask) {
                    List {
                        ForEach(self.users) { user in
                            Text("User")
                        }
                    }
                }
            }.alert(item: $alertType) {
                $0.alert
            }
        }.onAppear(perform: reloadUsers)
    }

    func reloadUsers() {
        guard let auth = authStatus.syncConfig else {
            alertType = .notLoggedIn
            return
        }
    }

    func add() {
    }

}

extension ShareView {
    func mergePublishers<T: Publisher>(_ publishers: [T]) -> AnyPublisher<T.Output, T.Failure> {
        publishers.reduce(Empty().eraseToAnyPublisher()) { acc, elem in
            acc.merge(with: elem).eraseToAnyPublisher()
        }
    }

    func taskWithReload<T: Publisher>(_ task: T) -> Cancellable where T.Output == Void, T.Failure == Error {
        task
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handleCompletion(completion:)) {
                self.reloadUsers()
        }
    }

    func handleCompletion(completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            return
        case .failure(let err as BadRequest):
            alertType = .requestError(err)
        case .failure(let err):
            alertType = .other(err)
        }
    }
}

extension ShareView {
    func getTokenWithGroupId() -> (token: String, groupId: String)? {
        guard let auth = authStatus.syncConfig else {
            return nil
        }

        guard let groupId = group.transform(defaultValue: nil, { $0.id }) else {
            return nil
        }

        return (auth.authToken, groupId)
    }

    func cancelTask() {
        task?.cancel()
        task = nil
    }
}

extension ShareView {
    enum AlertType: Identifiable {
        var id: ObjectIdentifier { .init(Self.self) }
        case notLoggedIn
        case removeSelf(() -> Void)
        case requestError(BadRequest)
        case other(Error)

        var alert: Alert {
            let title: String
            let message: Text?
            switch self {
            case .removeSelf(let cb):
                return Alert(title: Text("Do you really want to leave this group?"), primaryButton: .cancel(), secondaryButton: .destructive(Text("Leave group"), action: cb))
            case .notLoggedIn:
                title = "User is not logged in."
                message = nil
            case .requestError(let err):
                title = "Network error: \(err.code)"
                message = Text(err.body)
            case .other(let err):
                title = "Unown error"
                message = Text(err.localizedDescription)
            }
            return Alert(title: Text(title), message: message)
        }
    }
}

struct ShareView_Previews: PreviewProvider {
    static var previews: some View {
        ShareView(group: .init())
    }
}
