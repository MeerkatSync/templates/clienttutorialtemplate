//
//  Tmp.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 25/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

struct Tmp: Identifiable {
    var id: ObjectIdentifier = .init(Tmp.self)
}
