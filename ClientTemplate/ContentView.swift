//
//  ContentView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatClientSyncController

struct ContentView: View {
    @State private var selection = 0

    @ObservedObject
    private var networkError = NetworkErrorObserver()

 
    var body: some View {
        TabView(selection: $selection) {
            FoldersView()
            .tabItem {
                VStack {
                    Image(systemName: "folder")
                    Text("Folders")
                }
            }
                .tag(0)

            SettingsView()
                .tabItem {
                    VStack {
                        Image(systemName: "gear")
                        Text("Settings")
                    }
                }
                .tag(1)
        }.alert(item: $networkError.error) { err in
            Alert(title: Text(err.description))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
