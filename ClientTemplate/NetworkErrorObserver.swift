//
//  NetworkErrorObserver.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 26/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import Foundation
import MeerkatClientSyncController

class NetworkErrorObserver: ObservableObject {
    @Published
    var error: NetworkError? = nil

    private func handleNetworkError(_ err: SendError) {
        switch err.error {
        case let err as BadRequest where err.code == 401:
            SyncAuthStatus.shared.authStatus = .loggedOut
            error = .notAuthenticated
        default:
            error = .other(err)
        }
    }

    enum NetworkError: Identifiable {
        var id: ObjectIdentifier { .init(Self.self) }

        case notAuthenticated
        case other(SendError)

        var description: String {
            switch self {
            case .notAuthenticated:
                return "Token expired, you need to log in"
            case .other(let err):
                return "Can't complete '\(err.action)' action because of error: '\(err.error)'"
            }
        }

    }
}
