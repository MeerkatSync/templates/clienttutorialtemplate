//
//  LogInView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 29/02/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatClientSyncController

struct LogInView: View {
    let isRegistration: Bool

    let close: () -> Void

    @UserDefaultOptionalWrapper(.username)
    private var storedUsername: String?

    @ObservedObject
    private var syncStatus = SyncAuthStatus.shared

    @State private var username = ""
    @State private var password = ""
    @State private var password2 = ""
    @State private var task: Cancellable? = nil

    func login() {
        guard username.count >= 3 else {
            error = .shortUsername
            return
        }
    }

    func register() {
        guard username.count >= 3 else {
            error = .shortUsername
            return
        }
        guard password.count >= 8 else {
            error = .shortPassword
            return
        }
        guard password == password2 else {
            error = .passwordsMismatch
            return
        }
    }

    @State private var error: ReqError? = nil

    var body: some View {
        LoadingView(isShowing: .constant(false), cancel: cancelTask) {
            VStack {
                TextField("Username", text: self.$username)
                    .modifier(FormRowModifier())
                SecureField("Password", text: self.$password)
                    .modifier(FormRowModifier())
                if self.isRegistration {
                    SecureField("Password again", text: self.$password2)
                        .modifier(FormRowModifier())
                }
                HStack {
                    Spacer()
                    if self.isRegistration {
                        Button(action: self.register) {
                            Text("Register")
                                .modifier(FormButtonModifier(color: .orange))
                                .padding(.top, 20)
                        }

                    } else {
                        Button(action: self.login) {
                            Text("Login")
                                .modifier(FormButtonModifier(color: .green))
                                .padding(.top, 20)
                        }
                    }
                    Spacer()
                }
                Spacer()
            }.padding().alert(item: self.$error) { (err) -> Alert in
                Alert(title: Text(err.description))
            }
        }
    }

    func cancelTask() {
        task?.cancel()
    }
}

extension LogInView {
    private enum ReqError: Error, Identifiable, CustomStringConvertible {
        var id: String {
            description
        }

        var description: String {
            switch self {
            case .requestError(let r):
                return "Error: Response status code: \(r.code), body: \(r.body)"
            case .passwordsMismatch:
                return "Passwords are different"
            case .shortUsername:
                return "Username is too short (at least 3 characters)"
            case .shortPassword:
                return "Password is too short (at least 8 characters)"
            case .other(let err):
                return "\(err)"
            }
        }

        case requestError(BadRequest)
        case passwordsMismatch
        case shortPassword
        case shortUsername
        case other(Error)
    }
}

struct LogInView_Previews: PreviewProvider {
    static var previews: some View {
        LogInView(isRegistration: true) { }
    }
}
