//
//  FolderDetailView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct FolderDetailView: View {

    @State
    private var isEditing: SheetType? = nil

    let notes: [Tmp] = []

    var body: some View {
        VStack {
            List {
                ForEach(notes, id: \.id) { note in
                    Button(action: {
                        self.isEditing = .editNote("")
                    }) {
                        Text("Note")
                    }
                }.onDelete(perform: deleteNotes)
            }.navigationBarTitle("Folder")
                .navigationBarItems(trailing: HStack {
                    Button(action: {
                        self.isEditing = .share
                    }) {
                        Image(systemName: "person.3")
                    }.padding(.trailing)
                    Button(action: {
                        self.isEditing = .addNote
                    }) {
                        Image(systemName: "plus")
                    }
                })

        }.sheet(item: $isEditing) { sheet in
            sheet.view() {
                DispatchQueue.main.async {
                    self.isEditing = nil
                }
            }
        }
    }

    func deleteNotes(indexes: IndexSet) {       
    }
}

extension FolderDetailView {
    enum SheetType: Identifiable {
        var id: ObjectIdentifier { .init(SheetType.self) }

        case addNote
        case share
        case editNote(String)

        func view(close: @escaping () -> Void) -> some View {
            switch self {
            case .addNote:
                return AnyView(NoteView())
            case .share:
                return AnyView(ShareView(group: .init()))
            case .editNote(let n):
                return AnyView(NoteView())
            }
        }
    }
}

struct FolderDetail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            FolderDetailView()
        }
    }
}
