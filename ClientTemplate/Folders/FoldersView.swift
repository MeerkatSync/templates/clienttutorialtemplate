//
//  FoldersView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct FoldersView: View {

    @State
    private var activeFolder: String? = nil

    let folders: [Tmp] = []

    @State
    private var newFolder = false

    var body: some View {
        NavigationView {
            List {
                ForEach(folders) { folder in
                    Text("folder")
                }
                .onDelete(perform: deleteFolders)
            }.navigationBarTitle("Folders")
            .navigationBarItems(trailing: Button(action: {
                self.newFolder = true
            }) {
                Image(systemName: "folder.badge.plus")
            })
                .sheet(isPresented: $newFolder) {
                    NewFolderView()
            }
        }
    }

    func deleteFolders(indexes: IndexSet) {
    }
}

struct FoldersView_Previews: PreviewProvider {
    static var previews: some View {
        FoldersView()
    }
}
