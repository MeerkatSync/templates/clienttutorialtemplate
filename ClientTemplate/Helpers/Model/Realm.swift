//
//  Realm.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import RealmSwift

let RealmDefaultConfig: () -> Realm.Configuration = { .init(deleteRealmIfMigrationNeeded: true) }

func getRealm() -> Realm {
    return try! Realm(configuration: RealmDefaultConfig())
}
