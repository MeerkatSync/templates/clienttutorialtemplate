//
//  UserDefaultsWrapper.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatCore
import Foundation

@propertyWrapper
struct UserDefaultWrapper<T> {
    let inner: UserDefaultOptionalWrapper<T>
    let defaultValue: T

    init(_ key: String, defaultValue: T) {
        inner = .init(key)
        self.defaultValue = defaultValue
    }

    init(_ elem: UserDefaults.Element<T>) {
        inner = .init(elem.key)
        self.defaultValue = elem.defaultValue
    }

    var wrappedValue: T {
        get {
            inner.wrappedValue ?? defaultValue
        }
        nonmutating set {
            inner.wrappedValue = newValue
        }
    }
}

@propertyWrapper
struct UserDefaultCodableOptionalWrapper<T: Codable> {
    let inner: UserDefaultOptionalWrapper<Data>
    let defaultValue: T?

    init(_ key: String) {
        inner = .init(key)
        defaultValue = nil
    }

    init(_ elem: UserDefaults.Element<T?>) {
        inner = .init(elem.key)
        self.defaultValue = elem.defaultValue
    }

    var wrappedValue: T? {
        get {
            guard let data = inner.wrappedValue else {
                return defaultValue
            }

            return (try? JSONDecoder().decode(T.self, from: data)) ?? defaultValue
        }
        nonmutating set {
            if let v = newValue, let json = try? JSONEncoder().encode(v) {
                inner.wrappedValue = json
            } else {
                inner.wrappedValue = nil
            }
        }
    }
}

@propertyWrapper
struct UserDefaultOptionalWrapper<T> {
    let key: String
    let defaultValue: T?

    init(_ key: String) {
        self.key = key
        defaultValue = nil
    }

    init(_ elem: UserDefaults.Element<T?>) {
        self.key = elem.key
        self.defaultValue = elem.defaultValue
    }

    var wrappedValue: T? {
        get {
            return (UserDefaults.standard.object(forKey: key) as? T?) ?? defaultValue
        }
        nonmutating set {
            if let v = newValue {
                UserDefaults.standard.set(v, forKey: key)
            } else {
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
    }
}

extension UserDefaults {
    struct Element<T> {
        let key: String
        let defaultValue: T
    }
}

extension UserDefaults.Element where T == String? {
    static let username = UserDefaults.Element<T>(key: "username", defaultValue: nil)
}

public typealias SyncAuth = SyncIDTokenResponseBody
extension UserDefaults.Element where T == SyncAuth? {
    static let syncAuth = UserDefaults.Element<T>(key: "sync_auth", defaultValue: nil)
}

