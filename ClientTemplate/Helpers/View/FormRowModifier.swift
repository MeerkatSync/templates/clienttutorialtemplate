//
//  FormRowModifier.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI

struct FormRowModifier: ViewModifier {
    @Environment(\.colorScheme) private var colorScheme: ColorScheme

    var labelColor: Color {
        if colorScheme == .dark {
            return Color(white: 0.2)
        } else {
            return Color(white: 0.95)
        }
    }

    func body(content: Content) -> some View {
        content
            .padding(.horizontal)
            .padding(.vertical, 6)
            .background(labelColor)
            .cornerRadius(5)
    }


}
