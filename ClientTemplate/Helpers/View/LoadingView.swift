//
//  LoadingView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 29/02/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI

struct LoadingView<Content>: View where Content: View {
    @Binding var isShowing: Bool
    var cancel: (() -> Void)? = nil
    var content: () -> Content

    var body: some View {
        ZStack(alignment: .center) {
            self.content()
                .disabled(self.isShowing)
                .blur(radius: self.isShowing ? 3 : 0)
            VStack {
                Text("Loading...")
                    .font(.title)
                ActivityIndicator(isAnimating: .constant(true), style: .large)
                Button(action: {
                    self.cancel?()
                }) {
                    Text("Cancel")
                        .foregroundColor(.blue)
                        .padding(.top, 10)
                }
            }.padding()
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(self.isShowing ? 1 : 0)
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView(isShowing: .constant(true)) {
            Text("something")
        }
    }
}
