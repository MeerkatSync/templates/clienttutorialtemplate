//
//  NoteView.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 24/04/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import SwiftUI
import MeerkatRealmWrapper

struct NoteView: View {
    @Environment(\.presentationMode) var present

    init() {
        isEditing = false
        _title = .init(wrappedValue: "")
        _text = .init(wrappedValue: "")
    }

    let isEditing: Bool
    @State
    private var objectChanged: Bool = false

    @State
    private var title: String

    @State
    private var text: String

    var body: some View {
        NavigationView {
            VStack {
                TextField("Title", text: $title)
                    .modifier(FormRowModifier())
                TextField("text", text: $text)
                    .modifier(FormRowModifier())
                Button(action: save) {
                    Text("Save")
                }.modifier(FormButtonModifier(color: .green))
                Spacer()
            }.padding()
            .alert(isPresented: $objectChanged) {
                Alert(title: Text("Note is updated!"),
                      message: Text("There is updated version of note. Do you want to reload?"),
                      primaryButton: .destructive(Text("Reload"), action: reload),
                      secondaryButton: .cancel(Text("Save current version"), action: save))
            }.navigationBarTitle(isEditing ? title : "New note")

        }
    }

    func didChange() {
    }

    func reload() {
    }

    func save() {
        if isEditing {
            update()
        } else {
            saveNewNote()
        }
        close()
    }

    func update() {
    }

    func saveNewNote() {
    }

    func closeAlert() {
        objectChanged = false
    }

    func close() {
        self.present.wrappedValue.dismiss()
    }
}

struct NoteView_Previews: PreviewProvider {
    static var previews: some View {
        NoteView()
    }
}
