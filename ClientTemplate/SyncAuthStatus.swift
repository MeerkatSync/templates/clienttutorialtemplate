//
//  SyncAuthStatus.swift
//  ClientTemplate
//
//  Created by Filip Klembara on 11/03/2020.
//  Copyright © 2020 Filip Klembara. All rights reserved.
//

import MeerkatClientSyncController

class SyncAuthStatus: ObservableObject {

    static var shared: SyncAuthStatus = .init()

    @UserDefaultCodableOptionalWrapper(.syncAuth)
    private static var syncAuth: SyncAuth?

    private init() {
        if let auth = SyncAuthStatus.syncAuth {
            authStatus = .loggedIn(auth: auth)
        } else {
            authStatus = .loggedOut
        }
    }

    enum AuthStatus {
        case loggedOut
        case loggedIn(auth: SyncAuth)
    }

    var isLoggedIn: Bool {
        guard case .loggedIn = authStatus else {
            return false
        }
        return true
    }

    var syncConfig: SyncConfig? {
        guard case let .loggedIn(auth) = authStatus else {
            return nil
        }
        return .init(authToken: auth.token, syncToken: auth.syncId)
    }

    @Published
    var authStatus: AuthStatus {
        didSet {
            guard case let .loggedIn(auth) = authStatus else {
                SyncAuthStatus.syncAuth = nil
                return
            }
            SyncAuthStatus.syncAuth = auth
        }
    }
}
